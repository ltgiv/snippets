# systemd shortcuts



## System

- **Reload management parameters:**
    `systemctl daemon-reload`
- **Enable specific service:**
    `systemctl enable NAME.service`
- **Disable specific service:**
    `systemctl disable NAME.service`



------



## List
- **Start when system boots:**
    `systemctl list-unit-files --state=enabled`
- **Currently Running:**
    `systemctl list-units --type=service --state=running`
- **Active (running or exited):**
    `systemctl list-units --type=service --state=active`



------



## Service

- **States**
    
    - **Start**
        `systemctl start NAME.service`
    - **Restart**
        `systemctl restart NAME.service`
    - **Stop**
        `systemctl stop NAME.service`
- **Overview**
    
    - `systemctl status NAME.service`
- **Log**
    
    - `journalctl --follow --unit=NAME.service`
    
    
