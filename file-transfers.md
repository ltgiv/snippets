# File Transfers

I tend to use an unprivileged, LXC-based container that runs under Proxmox on a high-speed connection, and is responsible for moving bulk data between servers as background tasks.



------



## Script template

For repeated transfers between destinations, I save Bash scripts in `/usr/local/bin/` based on the following template which provides for disconnecting and reconnecting at-will via [GNU Screen](https://en.wikipedia.org/wiki/GNU_Screen).  Once the script is running, you can use ^AD (CTRL-A-D) to disconnect, run the script again to reconnect, ^C (CTRL-C) to abort the transfer and drop into a shell, and ^D (CTRL-D) at the subsequent prompt after completion to exit.

[Download sync-template.bash](https://gitlab.com/ltgiv/snippets/-/blob/master/bin/sync-template.bash)



------



## rsync



### SFTP to SFTP transfer

Please note: this can also be accomplished with Rclone, and I give an example of SFTP to a cloud provider below.

```bash
rsync \
    --archive \
    --hard-links \
    --verbose \
    --progress \
    --partial \
    --rsh=/usr/bin/ssh \
    USER1@HOST1:/SOURCE/ \
    USER2@HOST2:/DESTINATION/ \
;
```



### Upload via SFTP to server

```bash
rsync \
    --archive \
    --hard-links \
    --verbose \
    --progress \
    --partial \
    --rsh=/usr/bin/ssh \
    /SOURCE/ \
    USER@HOST:/DESTINATION/ \
;
```



### Download via SFTP from server

```bash
rsync \
    --archive \
    --hard-links \
    --verbose \
    --progress \
    --partial \
    --rsh=/usr/bin/ssh \
    USER@HOST:/SOURCE/ \
    /DESTINATION/ \
;
```



### Local transfer

I use this command for moving bulk data between mounted volumes that experience slower than desired I/O.

```bash
rsync \
    --archive \
    --hard-links \
    --inplace \
    --verbose \
    --progress \
    --partial \
    /SOURCE/ \
    /DESTINATION/ \
;
```



### Transferring rsnapshot backups

Transferring [rsnapshot](https://rsnapshot.org/) backups is a little more exotic since it uses hard links to reduce storage (please see [inode numbers](https://linoxide.com/linux-command/linux-inode/) to better understand this) while making delta snapshots at regular intervals via [cron](https://en.wikipedia.org/wiki/Cron).  To transfer an entire set of backups while retaining these deltas with minimal space used, you'll need the following command:

```bash
rsync \
    --archive \
    --verbose \
    --progress \
    --partial \
    --compress \
    --hard-links \
    --delete \
    --numeric-ids \
    --rsh=/usr/bin/ssh \
    /SOURCE/ \
    /DESTINATION/ \
;
```



------



## Rclone



The following is what I use for offloading from one of my legacy NAS units via SFTP to the cloud without using extra storage:

```bash
rclone \
    --stats=1s \
    --verbose=1 \
    copy \
    nasServer:/SOURCE/ \
    cloudProvider:/DESTINATION/ \
;
```



------



—[Louis T. Getterman IV](https://thad.getterman.org/about)



## License

```
MIT License

Copyright (c) 2020 Louis T. Getterman IV

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
