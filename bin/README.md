# Snippets : bin

This directory houses utilities that don't need their own repository, many of which I create in tandem with my blog, [Thad.Getterman.org](https://Thad.Getterman.org/)

 - `bulk-transmission-uris.bash` — Add multiple Magnet URIs and/or HTTP(s) Torrent file URLs to [Transmission](https://transmissionbt.com) via [transmission-remote](https://linux.die.net/man/1/transmission-remote).
 - `ddns-to-nginx.py`— Used to update Dynamic DNS entries for Docker Compose-based Nginx [running under systemd](https://gitlab.com/ltgiv/snippets/tree/master/systemd-template-docker-compose) every minute via Cron.
 - `dnscc.bash` — Used to compile multiple DNS zone files together for Stack Exchange's [DNSControl](https://github.com/StackExchange/dnscontrol) via Docker.  More information can be found at my blog article, [DNSControl Compiler](https://thad.getterman.org/2017/12/11/dnscontrol-compiler).
 - `sync-template.bash` — Template for bulk transferring of data in a resumable session.  Please see [File Transfers](https://gitlab.com/ltgiv/snippets/-/blob/master/file-transfers.md) for more information.



------



## License

```
MIT License

Copyright (c) 2020 Louis T. Getterman IV

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
