#!/usr/bin/env bash
: <<'!COMMENT'

Blow chunks
Louis T. Getterman IV (@LTGIV)
https://Thad.Getterman.org/
https://gitlab.com/ltgiv/snippets/

Split a large file into a series of chunks and transport them to a remote destination with rclone.

!COMMENT

# Exit if error
set -e

# Large file to split
sourceName="/path/to/large/file.tar"

# Caching directory (default is to save in the same path as the original file)
# cacheDest="/tmp"
cacheDest="$( dirname ${sourceName} )"

# Destination
rcloneDest="remotePlatform:/path/to/store/chunks/"

# Total number of chunks (e.g. 3 TB file / 15 chunks = 200 GB chunks)
totalChunks=15

# Chunk range : start
# startChunk=5
startChunk=1

# Chunk range : ends
# endChunk=10
endChunk=${totalChunks}

# Incrementally split, transport, and then remove cached chunk.
while [ $startChunk -le $endChunk ]; do

	# e.g. /path/to/large/file.tar.1
	destName="${cacheDest}/`basename "${sourceName}"`.${startChunk}"

	echo "Splitting chunk ${startChunk} of ${totalChunks}."
	split --number=l/${startChunk}/${totalChunks} "${sourceName}" | pv --size=$(( $( stat --printf="%s" "${sourceName}" ) / ${totalChunks} )) > "${destName}"

	echo "Transporting chunk ${startChunk}."
	rclone --stats=1s --verbose=1 copy "${destName}" "${rcloneDest}"

	echo "Removing chunk ${startChunk}."
	rm -rfv "${destName}";

	# Increment number
	startChunk=$(($startChunk+1))

done
