#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''

Louis T. Getterman IV : Snippets : DDNS to Nginx
https://gitlab.com/ltgiv/snippets/tree/master

1) Read why this is needed:
       * https://www.google.com/search?q=nginx+resolve+dynamic+dns
       * https://serverfault.com/questions/240476/how-to-force-nginx-to-resolve-dns-of-a-dynamic-hostname-everytime-when-doing-p

   See? What a pain - for now.

2) Save this file ( e.g. /usr/local/bin/ddns-to-nginx.py )

3) Add a Cron job (crontab -e) to run this every minute:
       * * * * * /usr/local/bin/ddns-to-nginx.py > /tmp/ddns-to-nginx.log

4) Optional: update the variables below to suit your needs.

'''

import subprocess
import os
import socket
import glob

# systemd service name - see https://gitlab.com/ltgiv/snippets/tree/master/systemd-template-docker-compose
serviceName	=	'docker-compose@web'

# Docker Compose path and specific service to restart.
cmdAction	=	'/usr/local/bin/docker-compose --file /etc/docker/compose/web/docker-compose.yaml restart server'

# Path to DDNS-related files under Nginx.
nginxPath	=	'/srv/nginx/etc/conf.d'

# Dynamically generated DNS entires (or you can override with your own list here.)
dnsEntries	=	[ os.path.basename( d )[ 5 : -4 ] for d in glob.glob( os.path.join( nginxPath, 'ddns-*.inc' ) ) ]

#----------------------------------- Nothing below this line needs to be changed

class ServiceMonitor(object):
	'''
	Script that monitors a service running on systemd. If service is not running the script will try to start the service.
	https://gist.github.com/piraz/de85b67e95132b5cf84e
	'''

	def __init__(self, service):
		self.service = service

	def is_active(self):
		"""Return True if service is running"""
		cmd = '/bin/systemctl status %s.service' % self.service
		proc = subprocess.Popen(cmd, shell=True,stdout=subprocess.PIPE)

		stdout_list = proc.communicate()[0].decode().split('\n')
		for line in stdout_list:
			if 'Active:' in line:
				if '(running)' in line:
					return True
		return False

	def start(self):
		cmd = '/bin/systemctl start %s.service' % self.service
		proc = subprocess.Popen(cmd, shell=True,stdout=subprocess.PIPE)
		proc.communicate()

	def stop(self):
		cmd = '/bin/systemctl stop %s.service' % self.service
		proc = subprocess.Popen(cmd, shell=True,stdout=subprocess.PIPE)
		proc.communicate()

	def restart(self):
		cmd = '/bin/systemctl restart %s.service' % self.service
		proc = subprocess.Popen(cmd, shell=True,stdout=subprocess.PIPE)
		proc.communicate()

changed	=	False
for currDNS in dnsEntries:

	currFile	=	os.path.join( nginxPath, f'ddns-{ currDNS.lower() }.inc' )

	try:

		currIP		=	socket.gethostbyname( currDNS )
		writeData	=	'allow {};\n'.format( currIP )

		# Touch file
		open( currFile, 'a' ).close()

		# File read and write
		with open( currFile, 'r+' ) as f:

			# Read and reset
			readData	=	f.read()
			f.seek( 0 )

			# File needs to be updated
			if ( readData != writeData ):
				f.write( writeData )
				f.truncate()
				print( "'{}' has been updated.".format( currDNS ) )
				changed	=	True
				pass # END IF

			pass # END WITH : CURRENT FILE

		pass # END : TRY

	except Exception as e:
		print( '{} : {}'.format( currDNS, e ) )
		pass # END : EXCEPTION

	pass # END FOR

monitor = ServiceMonitor( serviceName )

if changed and monitor.is_active():

	print( 'Restarting Nginx.' )
	proc	=	subprocess.Popen( cmdAction, shell=True, stdout=subprocess.PIPE )
	print( proc.communicate() )

	pass # END IF
