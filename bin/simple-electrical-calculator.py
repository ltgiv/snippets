#!/usr/bin/env python3
'''

Simple Electrical Calculator
Louis T. Getterman IV
Thad.Getterman.org

'''

import types, math

measurements	=	{
	'Pressure'		:	'Volts',
	'Current'		:	'Amperes',
	'Length'		:	'Meters',
	'Resistance'	:	'Ω',
	'Watts'			:	'Watts',
	'Resistivity'	:	'Ω',
	'Aream2'		:	'm²',
	'Areacm2'		:	'cm²',
	'Diameter'		:	'cm',
	'Diametermeters':	'm',
	'Radius'		:	'cm',
	'Eusize'		:	'mm²',
	'Maxcurrent'	:	'Amperes',
	'Awg'			:	'AWG',
}

'''

Thanks to Solar Wind for such an amazing wire gauge calculator!

https://www.solar-wind.co.uk/info/dc-cable-wire-sizing-tool-low-voltage-drop-calculator
https://solar-wind.co.uk/cable.js

'''
properties	=	[
	# E.U. Size,	Max current,	AWG Title
	( 0,			0,				'Too small'	),
	( 0.5,			5,				'20'		),
	( 1.5,			11,				'16'		),
	( 2.5,			17,				'14'		),
	( 4,			41,				'12'		),
	( 6,			53,				'10'		),
	( 10,			73,				'8'			),
	( 16,			99,				'6'			),
	( 25,			131,			'4'			),
	( 35,			192,			'2'			),
	( 50,			240,			'1'			),
	( 70,			297,			'2/0'		),
	( 95,			354,			'3/0'		),
	( 120,			414,			'4/0'		),
	( 150,			476,			'300MCM'	),
	( 185,			540,			'350MCM'	),
	( 240,			645,			'500MCM'	),
	( 300,			741,			'600MCM'	),
	( 400,			885,			'750MCM'	),
	( 500,			1025,			'1000MCM'	),
	( 630,			1190,			'Unknown'	),
	( -1,			-1,				'Too large'	),
]

def inchToMM( i ):
	return i * 25.4
	pass # END FUNCTION : Inches to Millimeters

def footToMeter( i ):
	return i / 3.281
	pass # END FUNCTION : Foot to Meter

def meterToFoot( i ):
	return i * 3.281
	pass # END

def calcCable( *, volts, amps, length, loss ):
	return round( ( ( length * amps * 0.04 ) / ( ( volts * loss ) / 100 ) ) * 100 ) / 100
	pass # END FUNCTION : Calculate cable

def findEntryEU( result ):

	entry	=	0
	found	=	False

	while ( not found and properties[ entry ][ 0 ] != -1 ):

		# Exact match
		if ( result == properties[ entry ][ 0 ] ):
			found	=	True
			pass # END IF

		else:

			if (
				( properties[ entry + 1 ][ 0 ] != -1 )
				and
				( properties[ entry ][ 0 ] < result < properties[ entry + 1 ][ 0 ] )
			):
				found	=	True
				pass # END IF

			entry	+=	1

			pass # END ELSE

		pass # END WHILE LOOP

	return entry

	pass # END FUNCTION : Find Entry : E.U.

def calcSize( table ):

	result	=	calcCable( volts=table.pressure, amps=table.current, length=table.length, loss=3 )
	index	=	findEntryEU( result )

	while ( properties[ index ][ 1 ] < table.current ) and ( properties[ index ][ 1 ] != -1 ):
		index += 1
		pass # END WHILE LOOP

	return index

	pass # END FUNCTION : Calculate size

table				=	types.SimpleNamespace()
default				=	types.SimpleNamespace()

# Defaults
default.pressure	=	12.0
default.current		=	1.0
default.length		=	1.0

# Input : Voltage
table.pressure		=	input( f'{"Pressure (D.C. or Single-Phase)": <32} [{default.pressure}] > ' )
table.pressure		=	float( table.pressure ) if table.pressure else default.pressure

# Input : Amperage
table.current		=	input( f'{"Current": <33} [{default.current}] > ' )
table.current		=	float( table.current ) if table.current else default.current

# Input : Length
table.length		=	input( f'{"Length (Meters)": <33} [{default.length}] > ' )
table.length		=	float( table.length ) if table.length else default.length

# Electrical calculations
table.resistance	=	round( ( table.pressure / table.current ), 2 )
table.watts			=	round( ( table.pressure * table.current ), 2 )

# Annealed Copper @ 20ºC : 1.72 ohm * 10^-8 meters 1.724e-8
table.resistivity	=	1.724 * 10**-8

# Cross-sectional area m² -> cm²
table.areaM2			=	( ( table.resistivity * table.length ) / table.resistance )
table.areaCM2			=	table.areaM2 * 100**2

# Radius in Centimeters
table.radius		=	math.sqrt( table.areaCM2 / math.pi )

# Diameter in Centimeters
table.diameter		=	table.radius * 2

# Diameter in Meters
table.diameterMeters=	table.diameter * 10

# FIX THIS
# table.conductance1	=	round( 1 / table.resistance, 2 )
# table.conductance2	=	round( 1 / table.resistivity, 2 )

# FIX THIS
# Wire Circular Mils - http://www.paigewire.com/pumpWireCalc.aspx
# table.cirmils1		=	( table.resistivity * 2 * table.current * meterToFoot( table.length ) ) / 0.03
# table.cirmils2		=	( table.resistivity * 2 * table.current * meterToFoot( table.length ) ) / ( table.areaCM2 / 100**2 )

# FIX THIS
# American Wire Gauge - https://en.wikipedia.org/wiki/American_wire_gauge
# table.gauge			=	( -39 * math.log( table.diameter * 10 /inchToMM( 0.005 ), 92 ) + 36 )

# FIX THIS — what does wire heat up to with resistance, and how much insulation will it need?
# table.temperature	=	?

# FIX THIS — https://www.rapidtables.com/calc/wire/voltage-drop-calculator.html
# table.voltageDrop	=	( table.current / ( table.resistance / footToMeter( 1_000 ) ) ) * meterToFoot( table.length )
# table.voltageDrop	=	table.current * ( 2 * table.length * table.resistivity / 1000 )

print( '-' * 79 )

calcRes				=	calcSize( table )

table.euSize		=	properties[ calcRes ][ 0 ]
table.maxCurrent	=	properties[ calcRes ][ 1 ]
table.awg			=	properties[ calcRes ][ 2 ]

for k,v in vars( table ).items():

	k	=	k.capitalize()

	print(

		f'{k: <15} =',
		( v if isinstance( v, str ) else '{0:.11f}'.format(v) ).rjust( 15 ),
		measurements.get( k, '' )

	)

	pass # END FOR

print( '-' * 79 )
