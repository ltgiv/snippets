#!/usr/bin/env bash
: <<'!COMMENT'

DNSControl Compiler v201907130712
Louis T. Getterman IV (@LTGIV)
https://Thad.Getterman.org/2017/12/11/dnscontrol-compiler

[ DNSControl ]
https://github.com/StackExchange/dnscontrol
https://github.com/StackExchange/dnscontrol/blob/master/docs/getting-started.md

[ Encrypt creds.json ]
https://www.agwa.name/projects/git-crypt/
https://github.com/StackExchange/blackbox

[ Thanks ]
https://github.com/StackExchange/dnscontrol/issues/51
https://stackoverflow.com/questions/24069173/recursive-cat-all-the-files-into-single-file

[ To-do ]
Other modes offered by DNSControl, such as:
	check
	print-ir
	version
	create-domains
	help
	[ utility ]
		create-domains - ensures that all domains in your configuration are present in all providers.
		get-certs - Issue certificates via Lets Encrypt

!COMMENT

################################################################################
SOURCE="${BASH_SOURCE[0]}"									# Dave Dopson, Thank You! - http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
while [ -h "$SOURCE" ]; do									# resolve $SOURCE until the file is no longer a symlink
	SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
	SOURCE="$(readlink "$SOURCE")"
	[[ $SOURCE != /* ]] && SOURCE="$SCRIPTPATH/$SOURCE"		# if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

SCRIPTPATH="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SCRIPTNAME=`basename "$SOURCE"`
################################################################################

# Compile
DNSCONFIGJS="${SCRIPTPATH}/dnsconfig.js"
cat "${SCRIPTPATH}/global.js" > "${DNSCONFIGJS}"
find "${SCRIPTPATH}/domains" -type f -name '*.js' -exec cat {} \; >> "${DNSCONFIGJS}"

# Mode
COMMAND=${1-preview}
case "$COMMAND" in

	"preview")
		echo "You're in preview mode, ^C to abort."
		echo "Please use '${SCRIPTNAME} push' when you're ready to save changes."
		sleep 2
		true
		;;

	"push")
		echo "Saving changes."
		true
		;;

	*)
		false
		;;

esac # END SWITCH

# Pull
docker pull stackexchange/dnscontrol

# Run
docker \
	run \
	--rm \
	-it \
	-v "${DNSCONFIGJS}":/dns/dnsconfig.js \
	-v "${SCRIPTPATH}/creds.json":/dns/creds.json \
	stackexchange/dnscontrol \
	dnscontrol ${COMMAND}

# Clean-up
rm -rf "${DNSCONFIGJS}"
