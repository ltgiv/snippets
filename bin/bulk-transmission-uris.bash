#!/usr/bin/env bash
: <<'!COMMENT'

Bulk Transmission URIs
Louis T. Getterman IV (@LTGIV)
https://Thad.Getterman.org/
https://gitlab.com/ltgiv/snippets/

Add multiple Magnet URIs and/or HTTP(s) Torrent file URLs to Transmission via `transmission-remote`.

!COMMENT

# Tranmission parameters
tPath='/MNT/EXAMPLE/DOWNLOADS/'						# Save path for Transmission
tSite='http://EXAMPLE.COM:9091/transmission/rpc/'	# Protocol, host, port, and path
tUser='EXAMPLE-USERNAME'							# Username
tPass='EXAMPLE-PASSWORD'							# Password (optional)
tArgs=''											# Additional arguments (e.g. --ssl if using HTTPS)

#---------------------------------- NOTHING BELOW THIS LINE NEEDS TO BE CHANGED.

control_c() {
	echo -e >&2 "\nOperation aborted."; exit 1;
}
trap control_c SIGINT

# Check for the existence of `transmission-remote`
hash 'transmission-remote' 2>/dev/null || { echo >&2 "'transmission-remote' is required.  Aborting."; exit 1; }

# User pastes magnets in.
echo -e "Magnet URIs and/or HTTP(s) Torrent file URLs to add, ^D when finished:\n"
magnets=()
while IFS= read -r LINE || [[ -n "$LINE" ]]; do
	if [ ! -z "$LINE" ]; then
		magnets+=( "${LINE}" )
	fi
done
echo; # add a line break to make it easier to read from all of the Magnet URI paste(s).

# If there's 1+ magnets to process, then we'll attempt to add.
if [ "${#magnets[@]}" -gt 0 ]; then

	# Parse each magnet entry, and wrap it with the appropriate flag.
	addArgs=""
	for i in ${magnets[@]}; do
		addArgs="${addArgs} --add '${i}'"
	done

	# Authentication arguments
	authArg=""
	if [ ! -z "${tUser}" ] && [ ! -z "${tPass}" ]; then
		authArg="--auth ${tUser}:${tPass}"
	elif [ ! -z "${tUser}" ]; then
		authArg="--auth ${tUser}"
	fi

	# Run command with parameters
	eval transmission-remote \
		\
		"${tSite}" \
		"${authArg}" \
		--download-dir "${tPath}" \
		\
		"${addArgs}" \
		\
		"${tArgs}" \
		;

else

	echo >&2 "No torrents were added."; exit 1;

fi
