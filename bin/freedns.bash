#!/usr/bin/env bash
: <<'!COMMENT'

Hooking FreeDNS to Linux network states
https://thad.getterman.org/articles/hooking-freedns-to-linux-network-states/

Louis T. Getterman IV

!COMMENT

# Variables
watchNIC="eth0"
freeDNShash="ValueGoesHereIncludingDoubleEqualSigns"
lockFile="/run/lock/freeDNS"
wait=300

if [ "$IFACE" = "${watchNIC}" ]; then
    (
        flock --nonblock 9 || exit 1
            /usr/bin/curl --interface ${watchNIC} --silent "https://freedns.afraid.org/dynamic/update.php?${freeDNShash}"
            sleep ${wait}
    ) 9>"${lockFile}"
fi

exit 0
