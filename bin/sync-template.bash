#!/usr/bin/env bash
: <<'!COMMENT'

Louis T. Getterman IV : snippets : file transfers
https://gitlab.com/ltgiv/snippets/-/blob/master/file-transfers.md

https://Thad.Getterman.org/about

Usage:
sync-template.bash

MIT License

Copyright (c) 2020 Louis T. Getterman IV

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

!COMMENT

# Set this variable to a unique session name, based upon your goal.
xferSsn='myName'

# Set this variable to one of the commands below — this will work with rsync and Rclone.
xferCmd="\
(rsync or rclone) \
    ONE ARGUMENT PER LINE \
    SOURCE \
    DESTINATION \
"

screen -rd ${xferSsn} 2>&1 >/dev/null
if [ $? -ne 0 ]; then

    screen -S ${xferSsn} sh -c " \
        echo "^AD to disconnect from your session, and '${0}' to reconnect to it."; \
        echo; \
        echo '[ Command ]'; \
        echo '${xferCmd}'; \
        echo; \
        sleep 2; \
        ${xferCmd}; \
        echo; \
        echo '^D to terminate this session.'; \
        echo; \
        bash; \
    "

fi
