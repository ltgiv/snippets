# systemd template for Docker Compose

## Install

    bash <( curl --silent https://gitlab.com/ltgiv/snippets/raw/master/systemd-template-docker-compose/install.bash )

## Create an example project
Using ***web*** as our project name, with a base Nginx installation:

**Create directory:**

    mkdir -pv /etc/docker/compose/web

**Create Docker Compose file:**

    touch /etc/docker/compose/web/docker-compose.yaml

**Save the following contents to this file:**

    ---
    
    version: '3'
        
    services:
    
        server:
            container_name: nginx
            image: nginx:latest
            restart: always

            ports:
                - 80:80      # HTTP
                - 443:443    # HTTPS

## Enable and start project

    systemctl enable docker-compose@web
    
    systemctl start docker-compose@web

## Check your new project's status

    systemctl status docker-compose@web
    
    journalctl -f -u docker-compose@web

> Written with [StackEdit](https://stackedit.io/).
