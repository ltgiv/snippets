#!/usr/bin/env bash
: <<'!COMMENT'

[ URL ]
https://gitlab.com/ltgiv/snippets/tree/master/systemd-template-docker-compose

[ Install ]
bash <( curl --silent https://gitlab.com/ltgiv/snippets/raw/master/systemd-template-docker-compose/install.bash )

[ Usage ]

	[ Create compose project ]
	mkdir -pv /etc/docker/compose/NAME

	[ Enable and start ]
	systemctl enable docker-compose@NAME
	systemctl start docker-compose@NAME

	[ Check status ]
	systemctl status docker-compose@NAME
	journalctl -f -u docker-compose@NAME

!COMMENT

composePath="/etc/docker/compose"

function setup {
	echo "Create directory : ${composePath}"
	mkdir -pv "${composePath}"
	chmod -v 750 "${composePath}"
}

function download {
	echo "${1} → ${2}"
	curl --output "$2" "$1"
	chmod -v 644 "$2"
}

function enable {
	systemctl daemon-reload
	systemctl enable docker-cleanup.timer
	systemctl start docker-cleanup.timer
}

function main {
	setup;

	download \
		"https://gitlab.com/ltgiv/snippets/raw/master/systemd-template-docker-compose/docker-compose@.service" \
		"/etc/systemd/system/docker-compose@.service" \
		;

	download \
		"https://gitlab.com/ltgiv/snippets/raw/master/systemd-template-docker-compose/docker-cleanup.service" \
		"/etc/systemd/system/docker-cleanup.service" \
		;

	download \
		"https://gitlab.com/ltgiv/snippets/raw/master/systemd-template-docker-compose/docker-cleanup.timer" \
		"/etc/systemd/system/docker-cleanup.timer" \
		;

	enable;
}

# Used to prevent incomplete bash scripts from running
main;
